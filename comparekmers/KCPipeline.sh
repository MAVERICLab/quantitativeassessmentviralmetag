#!/bin/sh

## written by Bonnie Hurwitz
## bonnie.hurwitz@gmail.com

## prerequisite:
## you must create the suffix arrays prior to this run
## like so:
## cd $FADIR/home/bhurwitz/biotools/vmerstat64/mkvtree -db $FILE1 -pl -allout -smap /home/bhurwitz/bioinfo/biotools/vmatch.distribution/vmatch_symbol_map -v
## run the vmatch analysis to create the mer file
## /home/bhurwitz/biotools/vmerstat64/vmerstat -indexname $FILE1.vm -minocc 2 -counts $FILE1

## This pipeline runs a comparison of kmers in a set of sequencess
## and generates information on the number of sequences that are
## shared between datasets in a pairwise fashion 
##  1 - create a configuration file for vmatch for each file to compare 
##  2 - run vmatch to do a pairwise kmer comparison b/w each fasta file 
##  3 - (optional) compile the kmer data   

## Need to modify the path below
## The name of the directory you want to files written to
## Date = year - month - day
## I name the directory but run date, but it could be anything
DATE="20120214"
FINALDIR="/home/bhurwitz/bioinfo/protein_universe/kmer_compare/$DATE"

## Need to modify the path below
## The name of the direcotry that contains the fasta files you want to 
## compare.
## There can be multiple files here by sample (or sets of sequences you wish to
## compare).  Each fasta file will be compared to every other fasta file in
## this directory
FADIR="/home/bhurwitz/bioinfo/data/processed/pacific_ocean"


## setting up the env variables for the run
SCRIPTS="$FINALDIR/scripts"
cd $FINALDIR
export CWD=$PWD
export DATE=$DATE
export FADIR=$FADIR
export SCRIPTS=$SCRIPTS
export FINALDIR=$FINALDIR
COUNT=0

# Need to modify the vmatch path here
# creating the suffix arrays
cd $FADIR
for file1 in `ls *fa`; do
   export FILE1=$file1

   # create the conf file
   VMATCH="/home/bhurwitz/biotools/vmerstat64/vmersearch"
   VMCONF="$FADIR/$FILE1.vmersearch.conf" 
   MERFILE=$FILE1.vm
   echo "$FADIR/$MERFILE" > $VMCONF
   echo "$VMATCH" >> $VMCONF
   echo "-strand fp" >> $VMCONF
   echo "-output qseqnum qpos counts sequence" >> $VMCONF


   # run a kmer comparison versus all files
   for file2 in `ls *fa`; do
      export FILE2=$file2
      echo "$file1 $file2"

      # mdr prefilter the sequences to have at least one 20mer > 2
      # in the suffis array
      NAME0="a.$FILE1.$FILE2"
      qsub -pe pthreads 8 -N $NAME0 -e $FINALDIR/err0 -o $FINALDIR/out0 -v FINALDIR,SCRIPTS,FILE1,FILE2,CWD,DATE,FADIR $SCRIPTS/run_vmatch.sh | sed 's/Your job //' | sed 's/ .*//'  > $CWD/$NAME0

      #get the mode for mers in each sequence bin the sequences by their mode
      NAME1="b.$FILE1.$FILE2"
      qsub -pe pthreads 8 -hold_jid `cat $CWD/$NAME0` -N $NAME1 -e $FINALDIR/err1 -o $FINALDIR/out1 -v FINALDIR,SCRIPTS,FILE1,FILE2,CWD,DATE,FADIR $SCRIPTS/run_post-vmatch.sh | sed 's/Your job //' | sed 's/ .*//'  > $CWD/$NAME1

      mv $FINALDIR/$NAME0 $FINALDIR/$NAME1 $FINALDIR/jid
   
   done
done

# extra analyses that can be run after the kmer analysis
# to create a unique set of read to read comparisons between samples
# based on the kmer data
# comment the for loop above out
#qsub -pe pthreads 8 -N kmer -e $FINALDIR/err2 -o $FINALDIR/out2 -v FINALDIR,SCRIPTS $SCRIPTS/run_create_index.sh

