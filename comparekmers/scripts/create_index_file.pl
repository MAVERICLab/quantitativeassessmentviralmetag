#! /opt/rocks/bin/perl -w

# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com
# create an index of read to read kmer info between samples
# gives unique pairs, no dups

use strict;

if ( @ARGV != 2 ) { die "Usage: prg dir outdir\n"; }

my $dir    = shift @ARGV;
my $outdir = shift @ARGV;

opendir( DIR, $dir ) or die $!;

my %files;
while ( my $file = readdir(DIR) ) {
    if ( $file =~ /\.mode$/ ) {
        my ( $compare_to, $fa1, $f, $fa2, $mode ) = split( /\./, $file );
        push( @{ $files{$f} }, $file );
    }
}

for my $ff ( keys %files ) {
    open( OUT, ">$outdir/$ff.kmercompare" ) || die "Cannot open out \n";
    my %file_to_seq_to_kmers = ();
    for my $file ( @{ $files{$ff} } ) {
        my ( $compare_to, $fa1, $f, $fa2, $mode ) = split( /\./, $file );
        open( F, "$dir/$file" ) || die "Cannot open file: $dir/$file\n";

        while (<F>) {
            chomp $_;
            my ( $id, $pos, $kmer ) = split( /\t/, $_ );
            $file_to_seq_to_kmers{$f}{$id}{$compare_to} = $kmer;
        }
    }

    my $first = 0;
    for my $f ( sort keys %file_to_seq_to_kmers ) {
        for my $id ( sort keys %{ $file_to_seq_to_kmers{$f} } ) {
            my @list  = ();
            my @kmers = ();
            for my $compare_to ( sort keys %{ $file_to_seq_to_kmers{$f}{$id} } )
            {
                push( @list,  $compare_to );
                push( @kmers, $file_to_seq_to_kmers{$f}{$id}{$compare_to} );
            }
            if ( $first == 0 ) {
                $first++;
                print OUT "file\tseqid\t", join( "\t", @list ), "\n";
            }
            print OUT "$f\t$id\t", join( "\t", @kmers ), "\n";
        }
    }

    close OUT;
}

