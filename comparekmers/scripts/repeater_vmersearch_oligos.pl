#! /opt/rocks/bin/perl 

# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com

# Purpose: Query an input set of oligomers
#          against a kmer suffix array database using vmersearch
#          and generate a tab delimited file with a
#          count for every oligo.
#          Note that suffix array generation via mkvtree and vmerstat
#          must be done before the use of this script
#          Options:
#          -c is the config file
#          -f is the fasta file of interest
#          -o output dir
#          -n is the kmer size

# use the following modules
use Getopt::Std;
use Bio::Seq;
use Bio::SeqIO;
use strict;

# get inputs
my %opts = ();
getopts( 'c:f:o:z:n:', \%opts );
my $conf   = $opts{'c'};
my $fasta  = $opts{'f'};
my $nmer   = $opts{'n'};
my $zfile  = $opts{'z'};
my $outdir = $opts{'o'};

# read fasta file and then reduce to only its name
my $seqin = Bio::SeqIO->new( -format => 'Fasta', -file => "$fasta" );

my $fasta_name;
if ( $fasta =~ /\//g ) {
    $fasta =~ m/.*\/(.*)$/;
    $fasta_name = $1;
}

else {
    $fasta_name = $fasta;
}

# parse the configuration
my @configuration = &configure($conf);
my $index         = shift @configuration;
my $vmersearch    = shift @configuration;

# do the search
print "Doing the vmersearch\n";
&search( $vmersearch, $fasta, $zfile, $index, $outdir, \@configuration );

##SUBS##

# run vmersearch
sub search {
    my $vmersearch = shift;
    my $fasta      = shift;
    my $fasta_name = shift;
    my $index      = shift;
    my $outdir     = shift;
    my $conf       = shift;
    my @conf       = @$conf;

    # build the command string
    my $command = "$vmersearch";
    foreach my $option (@conf) {
        $command .= " $option";
    }

    # input query file and give the suffix array database
    $command .= " $index";
    $command .= " $fasta";

    #issue the command
    `$command > $outdir/$fasta_name.repeats`;

    return (1);
}

# parse the configuration of vmatch and mkvtree
sub configure {
    my $conf = shift;

    # parse the conf file
    open( conf, "$conf" );
    my @conf;
    while ( my $line = <conf> ) {
        chomp $line;
        push( @conf, $line );
    }
    close(conf);
    return (@conf);
}
