#$ -S /bin/bash

## Run vmatch
## written by Bonnie Hurwitz
## bonnie.hurwitz@gmail.com
## This script finds kmers in common between a fasta file
## and a specified kmer index (created form another fatsa file)

cd $FADIR

## Note this is now done before the run!
## create the suffix array for this data set
## check that these paths are what you want
##/home/bhurwitz/biotools/vmerstat64/mkvtree -db $FILE1 -pl -allout -smap /home/bhurwitz/bioinfo/biotools/vmatch.distribution/vmatch_symbol_map -v
## run the vmatch analysis to create the mer file
## /home/bhurwitz/biotools/vmerstat64/vmerstat -indexname $FILE1.vm -minocc 2 -counts $FILE1

export INDEXDIR=$FADIR
export MERFILE=$FILE1.vm
export MER=20

cd $FINALDIR

VMCONF="$INDEXDIR/$FILE1.vmersearch.conf"
VMATCH="/home/bhurwitz/biotools/vmerstat64/vmersearch"

# run the MDR filter
FA="$FADIR/$FILE2"
MDRREAD="$FINALDIR/results/mdr_prefilter"
Z="$FILE1.$FILE2"

# create an ids file
egrep ">" $FADIR/$FILE2 | sed 's/>//' > $MDRREAD/$Z.ids

# here we create a file of mer frequencies 
$SCRIPTS/repeater_vmersearch_oligos.pl -c $VMCONF -f $FA -n $MER -o $MDRREAD -z $Z 
