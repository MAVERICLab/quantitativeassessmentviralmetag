#$ -S /bin/bash

##  Run create index 
## written by Bonnie Hurwitz
## bonnie.hurwitz@gmail.com


DIR1="$FINALDIR/results/mdr_prefilter"
DIR2="$FINALDIR/results/kmer_index"

# create_index 
$SCRIPTS/create_index_file.pl $DIR1 $DIR2
 
