#! /opt/rocks/bin/perl 

# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com
# get the mode for the 20mer frequency of each sequence

# use the following modules
use Getopt::Std;
use Bio::Seq;
use Bio::SeqIO;
use strict;
use lib
'/home/bhurwitz/bioinfo/perlcode/modules/Statistics-Descriptive-Discrete-0.07/blib/lib';
use Statistics::Descriptive::Discrete;

# get inputs
my %opts = ();
getopts( 'f:o:', \%opts );
my $fasta  = $opts{'f'};
my $outdir = $opts{'o'};

my $fasta_name;
if ( $fasta =~ /\//g ) {
    $fasta =~ m/.*\/(.*)$/;
    $fasta_name = $1;
}

else {
    $fasta_name = $fasta;
}

# need to look at kmers in the plus and minus direction
open( PLUS,   ">$outdir/$fasta_name.for_mode_plus" );
open( MINUS,  ">$outdir/$fasta_name.for_mode_minus" );
open( COUNTS, ">$outdir/$fasta_name.counts" );

# parse the vmersearch data
# here we will find the mode frequency for each sequence too
print "Parsing the vmersearch search\n";
my %counts;
my %for_mode_plus;
my %for_mode_minus;
open( OUT, "$outdir/$fasta_name.repeats" );
my $last_seqoffset = 0;
my @hold_counts    = ();
my $first          = 0;
LINE:

while ( my $line = <OUT> ) {
    chomp $line;
    next if ( $line =~ /^\#/ );

    ( my $seqoffset, my $meroffset, my $counts ) =
      split( /\t/, $line );

    if ( $first == 0 ) {
        $last_seqoffset = $seqoffset;
        $first++;
    }

    if ( $seqoffset != $last_seqoffset ) {

        #print and clear the hashes for memory
        $last_seqoffset = $seqoffset;

        for my $a ( keys %counts ) {
            print COUNTS "$a\t$counts{$a}\n";
        }
        %counts = ();

        for my $b ( keys %for_mode_plus ) {
            print PLUS "$b\t", join( " ", @{ $for_mode_plus{$b} } ), "\n";
        }
        %for_mode_plus = ();

        for my $c ( keys %for_mode_minus ) {
            print MINUS "$c\t", join( " ", @{ $for_mode_minus{$c} } ), "\n";
        }
        %for_mode_minus = ();

    }

    # get plus direction only for analysis
    if ( $meroffset =~ /\+/ ) {
        if ( $counts < 1000 ) {
            push( @{ $for_mode_plus{$seqoffset} }, $counts );
        }
        else {
            my $start = $meroffset;
            $start =~ s/\+//;
            my $end = $start + 20;
        }
    }
    if ( $meroffset =~ /\-/ ) {
        if ( $counts < 1000 ) {
            push( @{ $for_mode_minus{$seqoffset} }, $counts );
        }
        else {
            my $start = $meroffset;
            $start =~ s/\-//;
            my $end = $start + 20;
        }
    }

    $counts{$seqoffset} += $counts;
}
close(OUT);
close(COUNTS);
close(PLUS);
close(MINUS);

my %mode;
my %for_mode_plus     = ();
my %for_mode_minus    = ();
my %for_mode_plus_ct  = ();
my %for_mode_minus_ct = ();

open( PLUS,   "$outdir/$fasta_name.for_mode_plus" );
open( MINUS,  "$outdir/$fasta_name.for_mode_minus" );
open( COUNTS, "$outdir/$fasta_name.counts" );

while (<PLUS>) {
    chomp $_;
    my ( $seqnum, $values ) = split( /\t/, $_ );
    my @vals   = split( /\s/, $values );
    my $c      = @vals;
    my $stats1 = Statistics::Descriptive::Discrete->new;
    $stats1->add_data(@vals);
    my $mode_read_freq_plus = int( $stats1->mode() );
    $for_mode_plus{$seqnum}    = $mode_read_freq_plus;
    $for_mode_plus_ct{$seqnum} = $c;
}

while (<MINUS>) {
    chomp $_;
    my ( $seqnum, $values ) = split( /\t/, $_ );
    my @vals   = split( /\s/, $values );
    my $c      = @vals;
    my $stats1 = Statistics::Descriptive::Discrete->new;
    $stats1->add_data(@vals);
    my $mode_read_freq_minus = int( $stats1->mode() );
    $for_mode_minus{$seqnum}    = $mode_read_freq_minus;
    $for_mode_minus_ct{$seqnum} = $c;
}

while (<COUNTS>) {
    chomp $_;
    my ( $seqnum, $ct ) = split( /\t/, $_ );

    # get the mode frequency for the read
    my $mode_read_freq_plus     = 0;
    my $mode_read_freq_minus    = 0;
    my $mode_read_freq_plus_ct  = 0;
    my $mode_read_freq_minus_ct = 0;
    my $mode_read               = 0;
    if ( exists $for_mode_plus{$seqnum} ) {
        $mode_read_freq_plus    = $for_mode_plus{$seqnum};
        $mode_read_freq_plus_ct = $for_mode_plus_ct{$seqnum};
    }

    if ( exists $for_mode_minus{$seqnum} ) {
        $mode_read_freq_minus    = $for_mode_minus{$seqnum};
        $mode_read_freq_minus_ct = $for_mode_minus_ct{$seqnum};
    }

    if (   ( $mode_read_freq_plus_ct >= $mode_read_freq_minus_ct )
        && ( $mode_read_freq_plus_ct != 0 ) )
    {
        $mode_read = $mode_read_freq_plus;
    }

    if ( $mode_read_freq_minus_ct > $mode_read_freq_plus_ct ) {
        $mode_read = $mode_read_freq_minus;
    }

    # use the mode for the read direction with the greatest number of
    # mers
    $mode{$seqnum} = $mode_read;

}

# parse and combine with the fasta data
open( MODE, ">$outdir/$fasta_name.mode" );
open( IDS,  "$outdir/$fasta_name.ids" );
my $counter = 0;
while (<IDS>) {
    chomp $_;
    my $id = $_;
    if ( exists( $mode{$counter} ) ) {
        print MODE "$id\t$counter\t$mode{$counter}\n";
    }
    else {
        print MODE "$id\t$counter\t1\n";
    }
    $counter++;
}
close MODE;
