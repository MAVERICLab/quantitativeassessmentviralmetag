#$ -S /bin/bash

## Run post vmatch
## written by Bonnie Hurwitz
## a script to process vmatch results and find the mode value
## of the kmer between sets of samples for that read 

# configure the run
Z="$FILE1.$FILE2"
MDRREAD="$FINALDIR/results/mdr_prefilter"

cd $FINALDIR

# here we create bins of sequences by their mode frequency
$SCRIPTS/get_mode.pl -f $Z -o $MDRREAD

