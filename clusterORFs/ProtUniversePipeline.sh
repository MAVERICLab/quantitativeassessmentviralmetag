#!/bin/sh

## A pipeline to cluster ORFs
## first to a known set of clusters
## and then self-cluster the remaining
## written by Bonnie Hurwitz
## bonnie.hurwitz@gmail.com

# the date for the cluster run
# the directory the cluster run will be written to
DATE="20110906"
# make sure the "date" directory exists in FINALDIR 
DIR="/home/bhurwitz/bioinfo/protein_universe/create-prot-universe"
FINALDIR="$DIR/$DATE"
# the location of the ORF files to be clustered (fasta format)
FADIR="/home/bhurwitz/bioinfo/protein_universe/prot-from-selfcluster"


SCRIPTS="$FINALDIR/scripts"
cd $FINALDIR
export CWD=$PWD
export DATE=$DATE
export FADIR=$FADIR
export SCRIPTS=$SCRIPTS
export FINALDIR=$FINALDIR
export DIR=$DIR
COUNT=0

   # 1 - Cluster all ORFs within this set against current prot universe 
   #     (cdhit-2d)
   NAME1="a.$COUNT.$DATE"
   qsub -pe pthreads 24 -l vf=1.9G,h_vmem=46G,s_vmem=46G -N $NAME1 -e $FINALDIR/err2 -o $FINALDIR/out2 -v CWD,FADIR,DIR $SCRIPTS/run_pu-cluster.sh | sed 's/Your job //' | sed 's/ .*//' >$CWD/$NAME1

   # 2 - self cluster any ORFs that are not part of the old prot universe
   #     (cdhit)
   NAME2="b.$COUNT.$DATE"
   qsub -hold_jid `cat $CWD/$NAME1` -pe pthreads 24 -l vf=1.9G,h_vmem=46G,s_vmem=46G -N $NAME2 -e $FINALDIR/err3 -o $FINALDIR/out3 -v CWD $SCRIPTS/run_self-cluster.sh | sed 's/Your job //' | sed 's/ .*//' >$CWD/$NAME2 

   mv $CWD/a.* $CWD/b.* $CWD/jid
