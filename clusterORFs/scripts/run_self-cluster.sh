#$ -S /bin/bash

# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com

cd $CWD

# for cdhit 
PROGRAM="/home/bhurwitz/bioinfo/biotools/cd-hit-v4.5.5-2011-03-31/cd-hit"
INDIR="$CWD/results/cluster-curr-prot-univ/"
OUTDIR="$CWD/results/self-cluster/"
SCRIPTS="$CWD/scripts"
IDEN="0.6"
COV="0.8"
OPTIONS="-g 1 -n 4 -d 0 -T 24 -M 45000"

# run self clustering for each sample in the set
INFILE="$INDIR/cdhit60"
OUT="$OUTDIR/cdhit60"

# run cdhit
$PROGRAM -i $INFILE -o $OUT -c $IDEN -aS $COV $OPTIONS

# get 20+ id clusters and cluster to count FILE
OUTCL="$OUTDIR/cdhit60.clstr"
OUTFILE="$OUTDIR/cdhit60.20+"
$SCRIPTS/get_20+_clusters.pl $OUTCL $OUTFILE 

# create a list of ids that belong to the 20+ member clusters
$SCRIPTS/create_id_to_clst.pl $OUTCL $OUTFILE 

# create a fasta FILE with sequences from 20+ member clusters
LIST="$OUTFILE.clstr2id"
OUTFA="$OUTFILE.20+.fa"
$SCRIPTS/get_list_from_fa.pl $OUTFILE $LIST $OUTFA  
