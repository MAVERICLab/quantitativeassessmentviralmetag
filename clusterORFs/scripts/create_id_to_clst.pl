#! /opt/rocks/bin/perl -w

# create a list of ids to clusters
# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com

use strict;

my $in  = shift @ARGV;
my $out = shift @ARGV;

open( IN,  $in );
open( OUT, ">$out.clstr2id" );

my $curr_clstr;
my %cluster_to_count;
my $first = 0;
while (<IN>) {
    chomp $_;
    if ( $_ =~ /^>/ ) {
        my $clust = $_;
        $clust =~ s/>//;
        $first++;
        if ( $first > 1 ) {
            $curr_clstr = $clust;
        }
        else {
            $curr_clstr = $clust;
        }
    }
    else {
        push( @{ $cluster_to_count{$curr_clstr} }, $_ );
    }
}

for my $c ( sort { $cluster_to_count{$b} <=> $cluster_to_count{$a} }
    keys %cluster_to_count )
{
    my $size = @{ $cluster_to_count{$c} };
    for my $cl ( @{ $cluster_to_count{$c} } ) {
        $cl =~ s/.*>//;
        $cl =~ s/\..*//;
        $c  =~ s/\s+/_/;
        if ( $size >= 2 ) {
            print OUT "$c\t$cl\n";
        }
    }
}
