#! /opt/rocks/bin/perl -w

use strict;
use Bio::SeqIO;

###############################################################
# Purpose: to get a list of sequences from a fasta file
# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com
###############################################################

if ( @ARGV != 3 ) {
    die "Usage: get_list_from_fasta.pl input.fa list output\n";
}

my $inputfilename  = shift @ARGV;
my $list           = shift @ARGV;
my $outputfilename = shift @ARGV;

my $in = Bio::SeqIO->new(
    '-file'   => "$inputfilename",
    '-format' => 'Fasta'
);
my $out = Bio::SeqIO->new(
    '-file'   => ">$outputfilename",
    '-format' => 'Fasta'
);

open( LIST, $list );
my %list;
while (<LIST>) {
    chomp $_;
    my ( $cl, $id ) = split( /\t/, $_ );
    $list{$id} = $cl;
}

# go through each of the sequence records in Genbank
while ( my $seq = $in->next_seq() ) {
    my $id = $seq->id();
    if ( exists $list{$id} ) {
        $out->write_seq($seq);
    }
}

