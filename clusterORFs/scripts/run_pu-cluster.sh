#$ -S /bin/bash

# written by Bonnie Hurwitz
# bonnie.hurwitz@gmail.com

cd $CWD

# for cdhit 
PROGRAM="/home/bhurwitz/bioinfo/biotools/cd-hit-v4.5.5-2011-03-31/cd-hit-2d"
DB1="$DIR/current-prot-universe/current-prot-universe.fa"
DB2="$FADIR/all_proteins.fa"
OUTDIR="$CWD/results/cluster-curr-prot-univ"
SCRIPTS="$CWD/scripts"
IDEN="0.6"
COV="0.8"
OPTIONS="-g 1 -n 4 -d 0 -T 24 -M 45000"

# run pu clustering
OUT="$OUTDIR/cdhit60"

# run cdhit
$PROGRAM -i $DB1 -i2 $DB2 -o $OUT -c $IDEN -aS $COV $OPTIONS
