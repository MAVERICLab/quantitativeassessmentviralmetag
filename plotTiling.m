%% Description
%  this script tiles 454 DNA/cDNA reads along reference genome  
%  INPUT: BLASTN FORMAT -m 8 
%  OUTPUT: 2 PLOTS
%  1. mummer fragments aligned  (with CDS along top)
%  2. coverage index
%  You can zoom in (by changing 'axis' bounds) to see a region up close

%% Specify Params and Filenames %%%
G = 6102701;   % genome length
genomeInput = ['/Users/bonniehurwitz/Desktop/aproteo_BAL199_genome.txt'];
    % format should match line 21-22
blastInput = ['/Users/bonniehurwitz/Desktop/aproteo_BAL199_ststop.txt'];

%% Load files

% load blast 
[qstart qend rstart rend percentid] = textread(blastInput,...
    '%d%d%d%d%f','delimiter','\t','headerlines',0);
% load genome info 
[name strand gleft gright] = ...
textread(genomeInput,'%q%q%d%d',...
    'delimiter','\t','headerlines',0);

%%
for i = 1:length(gleft)     % this loop gets f/r strand info for plotting later
    if strcmp(strand(i),'+') == 1
        gstart(i) = gleft(i);
        gstop(i) = gright(i);
    else 
        gstart(i) = gright(i);
        gstop(i) = gleft(i);
    end
end
hitleft = zeros(length(rstart),1);     % preallocate 
hitright = zeros(length(rstart),1);
for i=1:length(rstart)
    if rstart(i)<rend(i)        
        rleft(i) = rstart(i);
        rright(i) = rend(i);
        percentid(i) = percentid(i)*100;
    else
        rleft(i) = rend(i);
        rright(i) = rstart(i);
        percentid(i) = percentid(i)*100;
    end
end

L = length(qstart);  % number of lines in blast file

c = zeros(G,1); % coverage at each point in genome

for i = 1:length(rstart)
    c(rleft(i):rright(i)) = c(rleft(i):rright(i)) + 1;
end

saveworkspace = ['aproteo_BAL199.ws'];
save(saveworkspace)             %% saves current workspace so we don't have to rerun!

figure; hold on
%%  plot coverage index

clear score level x lx w pos step k tmp 
w = 1000;
step = 500;
k=1;
pos = w/2 + 1;      % pos is center of window
while pos < (G-w/2)
    tmp = c((pos-w/2):(pos+w/2));
    score(k) = sum(tmp)/w;
    k=k+1;
    pos = pos + step;
end
x= [w/2+1:step:pos];
lx = length(x);

subplot(3,1,2)
ymax = 12;

plot(x(1:lx-1),score,'b-')
axis([0 G 0 ymax])

level(1:length(gleft),1:2) = 45;
line(transpose([gleft gright]),transpose(level),'LineWidth',8)
title('coverage in metagenomic dataset')
ylabel('coverage index')

%% plot mummer %ID for each alignment, tile reads

subplot(3,1,1)
    hold on;
axis([0 G 20 105])

for v = 1:length(rstart)
    plot([rleft(v) rright(v)],[percentid(v) percentid(v)],'b-')
end

level(1:length(gleft),1:2) = 102.5;
%%line(transpose([gleft gright]),transpose(level),'LineWidth',8)
title('environmental segments aligned to genome')
ylabel('%ID')
