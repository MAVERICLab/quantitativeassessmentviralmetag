# Quantitative assessment of concentration, purification and replication in viral metagenomics #

## Author ##

Bonnie Hurwitz

## CITATION ##

If you use this software, please cite [Hurwitz, B.L., Deng, L., Poulos, B.T., & Sullivan, M.B. (2012). Evaluation of methods to concentrate and purify ocean virus communities through comparative, replicated metagenomics. Environ Microbiol. 15(5), 1428–1440.](http://onlinelibrary.wiley.com/doi/10.1111/j.1462-2920.2012.02836.x/full)